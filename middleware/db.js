import { MongoClient } from 'mongodb';
import nextConnect from 'next-connect';

const {
  DB_PW
} = process.env;

const client = new MongoClient(`mongodb+srv://robbiesh:${DB_PW}@cluster0-5mrbi.mongodb.net/test?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function database(req, res, next) {
  if (!client.isConnected()) await client.connect();
  req.dbClient = client;
  req.db = client.db('subscriptions');
  return next();
}

const middleware = nextConnect();

middleware.use(database);

export default middleware;