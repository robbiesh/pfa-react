const { MongoClient } = require('mongodb');

const getSubscriptionLevel = async (ctx, accessToken, shop, DB_PW) => {
    const query = JSON.stringify({
      query: `query {
        currentAppInstallation {
          activeSubscriptions {
            name
            returnUrl
            id
            status
            createdAt
            trialDays
            test
          }
        }
      }`
    });
  
    const response = await fetch(`https://${shop}/admin/api/2019-07/graphql.json`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "X-Shopify-Access-Token": accessToken,
      },
      body: query
    })
  
    const responseJson = await response.json();
    const responseJsonArray = responseJson.data.currentAppInstallation.activeSubscriptions;
  
    if (responseJsonArray.length !== 0) {

      const client = new MongoClient(`mongodb+srv://robbiesh:${DB_PW}@cluster0-5mrbi.mongodb.net/test?retryWrites=true&w=majority`, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
      });

      let gq_status = responseJson.data.currentAppInstallation.activeSubscriptions[0].status;

      try {
          // Connect to the MongoDB cluster
          await client.connect();

          if (gq_status == 'ACTIVE') {
            await client.db("subscriptions").collection("Subscriptions").updateOne({ _id: shop}, { $set: {trial: false}}, {upsert: true});
          }

      } catch (e) {
          console.error(e);
      } finally {
          await client.close();
      }

      

    }

    return responseJson;
  };
  
  module.exports = getSubscriptionLevel;