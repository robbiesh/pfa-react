const { MongoClient } = require('mongodb');

const setSubscription = async (shop, DB_PW) => {

    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     */
    const client = new MongoClient(`mongodb+srv://robbiesh:${DB_PW}@cluster0-5mrbi.mongodb.net/test?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    try {
        // Connect to the MongoDB cluster
        await client.connect();

        let sub = {
            _id: shop,
            trial: true
        }

        let exists = await client.db("subscriptions").collection("Subscriptions").findOne({ _id: shop});

        console.log(exists);

        if (exists) {
            client.db("subscriptions").collection("Subscriptions").updateOne({ _id: shop}, { $set: {trial: true}}, {upsert: true});
        } else {
            // Make the appropriate DB call
            await createSubscription(client, sub);
        }

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

async function createSubscription(client, newSubscription){
    const result = await client.db("subscriptions").collection("Subscriptions").insertOne(newSubscription);
    console.log(`New listing created with the following id: ${result.insertedId}`);
}

module.exports = setSubscription;