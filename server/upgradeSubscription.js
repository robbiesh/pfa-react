const { MongoClient } = require('mongodb');

const upgradeSubscription = async (shop, DB_PW) => {

    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     */

    // console.log(DB_PW);

    const client = new MongoClient(`mongodb+srv://robbiesh:${DB_PW}@cluster0-5mrbi.mongodb.net/test?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    try {
        // Connect to the MongoDB cluster
        await client.connect();

        // Make the appropriate DB call
        await _upgradeSubscription(client, shop);

    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

async function _upgradeSubscription(client, shop){
    const result = await client.db("subscriptions").collection("Subscriptions").updateOne({ _id: shop}, { $set: {trial: false}}, {upsert: true});
    // console.log(`Subscription updated with the following id: ${result.updatedId}`);
    // console.log(result);
}

module.exports = upgradeSubscription;