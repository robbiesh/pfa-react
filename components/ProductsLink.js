import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import {
  Link,
} from '@shopify/polaris';

const GET_STORE_URL = gql`
  query {
    shop {
      myshopifyDomain
    }
  }
`;

class ProductsLink extends React.Component {

  render() {
    return (
      <Query query={GET_STORE_URL}>
        {({ data, loading, error }) => {
          if (loading) { return <span></span>; }
          if (error) { return <span>{error.message}</span>; }
          // console.log(data);
          let products_link = 'https://' + data.shop.myshopifyDomain + '/admin/products';

          return (
            <Link url={products_link} external={true}>{this.props.children}</Link>
          );
        }}
      </Query>
    );
  }
}

export default ProductsLink;