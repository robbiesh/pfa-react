require('isomorphic-fetch');
const dotenv = require('dotenv');

dotenv.config();

// const buf = Buffer.from('hello world')
// const opt = { debug: true }
// const config = dotenv.parse(buf, opt)

const Koa = require('koa');
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const session = require('koa-session');
const { default: graphQLProxy } = require('@shopify/koa-shopify-graphql-proxy');
const { ApiVersion } = require('@shopify/koa-shopify-graphql-proxy');
const Router = require('koa-router');
const { receiveWebhook, registerWebhook } = require('@shopify/koa-shopify-webhooks');
const getSubscriptionUrl = require('./server/getSubscriptionUrl');
const setSubscription = require('./server/setSubscription');
const getSubscriptionLevel = require('./server/getSubscriptionLevel');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const {
  SHOPIFY_API_SECRET_KEY,
  SHOPIFY_API_KEY,
  HOST,
  API_VERSION,
  DB_PW
} = process.env;

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.use(session({ sameSite: 'none', secure: true }, server));
  server.keys = [SHOPIFY_API_SECRET_KEY];

  server.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET_KEY,
      scopes: ['read_products'],
      async afterAuth(ctx) {
        const { shop, accessToken } = ctx.session;
        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });
        
        await setSubscription(shop, DB_PW);

        ctx.redirect('/');
      }
    })
  );

  router.get('/upgrade', async ctx => {
    const { shop, accessToken } = ctx.session;
    await getSubscriptionUrl(ctx, accessToken, shop, DB_PW);
  })

  server.use(graphQLProxy({ version: ApiVersion.April19 }));

  router.get('*', verifyRequest(), async (ctx) => {

    await handle(ctx.req, ctx.res);

    const { shop, accessToken } = ctx.session;
    await getSubscriptionLevel(ctx, accessToken, shop, DB_PW);

    ctx.respond = false;
    ctx.res.statusCode = 200;

  });

  server.use(router.allowedMethods());
  server.use(router.routes());

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});