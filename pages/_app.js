import App from 'next/app';
import Head from 'next/head';
import { Provider } from '@shopify/app-bridge-react';
import { AppProvider } from '@shopify/polaris';
import '@shopify/polaris/styles.css';
import translations from '@shopify/polaris/locales/en.json';
import Cookies from 'js-cookie';
import 'cross-fetch/polyfill';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';


const client = new ApolloClient({
  fetchOptions: {
    credentials: 'include'
  },
});

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    const config = { apiKey: API_KEY, shopOrigin: Cookies.get("shopOrigin"), forceRedirect: true };
    return (
      <React.Fragment>
        <Head>
          <title>Photo Finish</title>
          <meta charSet="utf-8" />
          <link rel="icon" type="image/png" href="https://cdn.shopify.com/s/files/1/0269/9850/5561/files/2020-05-03.png" />
        </Head>
        
        <AppProvider config={config} i18n={translations}>
          <ApolloProvider client={client}>
            <Component {...pageProps} />
          </ApolloProvider>
        </AppProvider>
        
        
      </React.Fragment>
    );
  }
}

export default MyApp;