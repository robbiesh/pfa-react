import nextConnect from 'next-connect';
import middleware from '../../middleware/db';

const handler = nextConnect();

handler.use(middleware);

handler.get(async (req, res) => {

    let cookies = parseCookies(req);
    let doc = await req.db.collection('Subscriptions').findOne({_id: cookies.shopOrigin});
    res.json(doc);
});


function parseCookies (request) {
    let list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        let parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}

export default handler;