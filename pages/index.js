import React, {useRef, useState, useCallback} from 'react';
import {ArrowLeftMinor, ConversationMinor, HomeMajorMonotone, QuestionMarkMajorMonotone} from '@shopify/polaris-icons';
import utils from '../utils.js';
import 'isomorphic-fetch';
var db = require('dropbox');
import { CSVLink, CSVDownload } from "react-csv";
import Cookies from 'js-cookie';
import ProductsLink from '../components/ProductsLink';
import {
  FooterHelp,
  Banner,
  Link,
  Checkbox,
  Select,
  Spinner,
  Button,
  Badge,
  Card,
  CalloutCard,
  Form,
  FormLayout,
  Layout,
  Page,
  Stack,
  DropZone,
  ActionList,
  AppProvider,
  ContextualSaveBar,
  Frame,
  Loading,
  Modal,
  Navigation,
  SkeletonBodyText,
  SkeletonDisplayText,
  SkeletonPage,
  TextContainer,
  TextField,
  Toast,
  TopBar,
  Thumbnail,
  Caption,
  SkeletonThumbnail
} from '@shopify/polaris';
// import { ResourcePicker, TitleBar } from '@shopify/app-bridge-react';
import Papa from 'papaparse';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

class Index extends React.Component {
  state = { 
    hasAuthCode: false,
    db_data: [],
    db_data_count: 0,
    has_db_data: false,
    csv_data: [],
    has_csv_data: false,
    csv_generated_data: [],
    has_generated_csv: false,
    is_trial: false,
    image_convention: 'sku',
    is_disabled: true,
    has_trial_response: false,
    shop_origin: Cookies.get("shopOrigin")
  }
  render() {

    return (
        <FrameOuter db_data_count={this.state.db_data_count} shop_origin={this.state.shop_origin} has_trial_response={this.state.has_trial_response} is_trial={this.state.is_trial} is_disabled={this.state.is_disabled} has_db_data={this.state.has_db_data} auth={this.state.hasAuthCode} db_handler={this.handleDB} csv_handler={this.handleCSV} compare={this.compareFiles} download={this.downloadCSV} image_convention={this.imageConvention}>
        </FrameOuter>
    );
  }
  componentDidMount() {

    // check if has dropbox authentication code

    if (isAuthenticated()) {
      this.setState({
        ...this.state,
        hasAuthCode: true
      });
    }

    let t = this;

    checkTrial().then(function(response) {
      t.setState({
        has_trial_response: true,
        is_trial: response
      });
    }).catch(function(error) {
      console.log(error);
    }); 

  }

  imageConvention = (val) => {
    this.setState({
      image_convention: val
    });
  };

  handleDB = (arr) => {
    this.setState({
      db_data: arr,
      has_db_data: true,
      db_data_count: arr.length
    });

    this.isDisabled();

  };

  isDisabled = () => {

    if (this.state.has_csv_data && this.state.has_db_data) {
      this.setState({
        is_disabled: false
      })
    }
  }

  handleCSV = (arr) => {
    this.setState({
      csv_data: arr,
      has_csv_data: true
    });

    this.isDisabled();
  };

// function comparing skus

  compareFiles = () => {

    let csv_data = this.state.csv_data;
    let db_data = this.state.db_data;

    let image_convention = this.state.image_convention;

    let new_csv = [];

    // add the header row first

    new_csv.push(csv_data[0]);

    // loop through every row of the csv
  
    for (let i = 1; i < csv_data.length; i++) {

        // set the current sku of the row

        let curr_sku;

        if (image_convention == 'barcode') {
          curr_sku = csv_data[i][23];
        } else {
          curr_sku = csv_data[i][13];
        }

        let first = true;

        if (curr_sku) {
          curr_sku = curr_sku.replace(/'/g, '')
        }

        if (csv_data[i][24]) {

          let new_row = JSON.parse(JSON.stringify(csv_data[i]));
          new_csv.push(new_row);

        } else {
         
            // at every row, loop through all the db images
          for (let j = 0; j < db_data.length; j++) {

            // set the db image name
            let db_image_name = db_data[j].name;

            // if dropbox image n (1234.png) includes csv SKU (1234)

            if (db_image_name.includes(curr_sku)) {
              
              if (first) {

                let new_row = JSON.parse(JSON.stringify(csv_data[i]));
                // let new_row = csv_data[i];
                new_row[24] = db_data[j].link;
                new_row[43] = db_data[j].link;

                new_csv.push(new_row);

                first = false;
              } else {
                let new_row = [];
                new_row[0] = csv_data[i][0];
                new_row[24] = db_data[j].link;

                new_csv.push(new_row);
              }
            }
          }

        }
  
        if (i == (csv_data.length - 1)) {

          let parsed_csv = Papa.unparse(new_csv, { quotes: true });

          this.setState({
            ...this.state,
            csv_generated_data: parsed_csv,
            has_generated_csv: true
          });
        }
    }  
  }

  downloadCSV = () => {

    let csvContent = "data:text/csv;charset=utf-8," + this.state.csv_generated_data;

    var hiddenElement = document.createElement('a');
    hiddenElement.href = encodeURI(csvContent);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'csv_with_images.csv';
    hiddenElement.click();

  }

}

function checkTrial() {
  return checkSubscription().then(function(response) {
    return response;
  }).catch(function(error) {
    console.log(error);
  })
}

async function checkSubscription() {
  const res = await fetch('https://app.photofinish.io/api/getSubscriptionLevel')
  const json = await res.json()

  return json.trial;
}


function UpgradeCardOuter(props) {

  if (props.has_trial_response) {
    return (
      <UpgradeCard has_trial_response={props.has_trial_response} is_trial={props.is_trial}>
      </UpgradeCard>
    );
  } else {
    return (
      <UpgradeLoading></UpgradeLoading>
    );
  }

}

function UpgradeLoading() {
  return (
    <Layout.Section>
      <Card sectioned>
        <SkeletonBodyText lines={6} />
      </Card>
    </Layout.Section>
  );
}

function UpgradeCard(props) {

  if (props.is_trial) {
    return (
      <Layout.Section>
        <Banner
          title="You are currently using a free trial."
          status="warning"
          action={{
            content: 'Upgrade',
            url: 'https://app.photofinish.io/upgrade',
          }}
        >
          <p>The free trial limits uploads to 20 images at a time. Upgrade to premium to upload unlimited images.</p>
        </Banner>
      </Layout.Section>
    );
  } else {
    return null;
  }
}

let CLIENT_ID = 'e7l22y611ouo5ai';

// Parses the url and gets the access token if it is in the urls hash
function getAccessTokenFromUrl() {
    return utils.parseQueryString(window.location.hash).access_token;
}


function ButtonConnect(props) {

  let dbx = new db.Dropbox({ clientId: CLIENT_ID, fetch: fetch });
  let authUrl = dbx.getAuthenticationUrl('https://app.photofinish.io');

  return (
    <div>
      {props.auth
        ? <ConnectResponse db_data_count={props.db_data_count} image_convention={props.image_convention} has_db_data={props.has_db_data} db_handler={props.db_handler} is_trial={props.is_trial}></ConnectResponse>
        : <ConnectButton authUrl={authUrl}></ConnectButton>
      }
    </div>
  );
}

function ConnectButton(props) {
  return (
    <Card sectioned>
      <Button url={props.authUrl} primary>Connect to Dropbox</Button>
    </Card>
  )
}

class ConnectResponse extends React.Component {

  state = { 
    is_clicked: false
  }

  clickHandler = () => {

    this.setState({
      is_clicked: true,
    });
  };

  getDBImages() {

    let button = this;

    this.clickHandler();
  
    let ACCESS_TOKEN = getAccessTokenFromUrl();
    let dbx = new db.Dropbox({ accessToken: ACCESS_TOKEN, fetch: fetch });
  
    dbx.filesListFolder({path: ''})
        .then(function(response) {
  
            return getTemporaryLinks(response.entries, dbx, button.props.is_trial)
                .then(function(result) {
                    return result;
                });
            
        })
        .then(function(result) {

          result.sort((a, b) => {
            if (a.name < b.name)
              return -1;
            if (a.name > b.name)
              return 1;
            return 0;
          });

          button.props.db_handler(result);
        })
        .catch(function(error) {
            console.error(error);
        });
  
  }

  render() {
    return (
      
      <Layout>
        <Layout.Section>
          <Banner
            title="Your Dropbox account has been authenticated."
            status="success"
            action={{
              content: 'Upload photos to Dropbox',
              url: 'https://www.dropbox.com/home/Apps/Photo%20Finish',
              external: true,
            }}
          >
            <TextContainer>
              <p>We've created a new folder in your Dropbox named <strong>Photo Finish</strong>. Upload your product photos to that folder.</p>
              <p>Name all photos using either the variant SKU or barcode. For example, if the SKU for a variant is 1234, name the photo <i>1234.png</i>. If you have two photos for variant 1234, name the photos <i>1234_1.png</i> and <i>1234_2.png</i>.</p>
            </TextContainer>
          </Banner>
          {/* <p>Your Dropbox account has been authenticated. <Link url='https://www.dropbox.com/home/Apps/Photo%20Finish' external={true}>Upload your product images.</Link></p> */}
        </Layout.Section>
        <Layout.Section>
          <Card>
            <Card.Section>
              <Stack vertical>
                <Stack.Item>
                  <Stack vertical>
                    <Stack.Item>
                      <Button primary onClick={this.getDBImages.bind(this)}>Get Photos</Button>
                    </Stack.Item>
                    <Stack.Item>
                      <DBButton db_data_count={this.props.db_data_count} has_db_data={this.props.has_db_data} is_clicked={this.state.is_clicked}>
                      </DBButton>
                    </Stack.Item>
                  </Stack>
                </Stack.Item>
              </Stack>
            </Card.Section>
            <SelectExample has_db_data={this.props.has_db_data} image_convention={this.props.image_convention}></SelectExample>
          </Card>
        </Layout.Section>
      </Layout>

    
      
    );
  };
}

function NewBanner() {
  return (
    <Banner>
      You can export the CSV from your <ProductsLink>store's Products page.</ProductsLink>
    </Banner>
  );
}

function DBButton(props) {

  return (
    <div>
      {props.has_db_data
        ? <BadgeSuccess db_data_count={props.db_data_count}></BadgeSuccess>
        : <BadgeResponse is_clicked={props.is_clicked}></BadgeResponse>
      }
    </div>
  );
}

function BadgeSuccess(props) {
  // console.log(props.db_data_count);

  return (
    <Badge status="success">{props.db_data_count} images successfully retrieved.</Badge>
  );
}

function BadgeResponse(props) {
  return (
    <div>
      {props.is_clicked
        ? <Spinner accessibilityLabel="Small spinner example" size="small" color="teal" />
        : <Badge status="attention">No images retrieved.</Badge>
      }
    </div>
  )
}


// If the user was just redirected from authenticating, the urls hash will
// contain the access token.
function isAuthenticated() {
    if (getAccessTokenFromUrl() !== undefined) {
        return true;
    } else {
        return false;
    }
}




// ----------------------------------


function getTemporaryLinks(files, dbx, is_trial) {

  let promises = [];

  function filesLength() {

    if (is_trial) {
      return 20;
    } else {
      return files.length;
    }
  }

  for (let i = 0; i < filesLength(); i++) {

      if (files[i]['.tag'] != 'folder') {

          let promise = dbx.filesGetTemporaryLink({"path": files[i].path_display})
              .then(function(response) {

                  const obj = {
                      'link': response.link,
                      'name': response.metadata.name
                  }

                  return obj;

              })
              .catch(function(error) {
                  console.error(error);
              });

          promises.push(promise);
      
      }
  }

  return Promise.all(promises).then(function(result) {
      return result;
  }).catch(function(e) {
      console.log(e);
  });

}


function DropZoneCSV(props) {
  const [file, setFile] = useState();

  const handleDropZoneDrop = useCallback(
    (_dropFiles, acceptedFiles, _rejectedFiles) => {
      setFile((file) => acceptedFiles[0]);
      // handleFile(acceptedFiles[0], props);

      Papa.parse(acceptedFiles[0], {
        complete: function(results) {
          props.csv_handler(results.data);
        }
      });
    },
    [],
  );

  const validFileTypes = ['text/csv'];

  const fileUpload = !file && <DropZone.FileUpload actionTitle='Add CSV' />;
  const uploadedFile = file && (
    <Stack>
      <Thumbnail
        size="small"
        alt={file.name}
        source={
          validFileTypes.indexOf(file.type) > 0
            ? window.URL.createObjectURL(file)
            : 'https://cdn.shopify.com/s/files/1/0757/9955/files/New_Post.png?12678548500147524304'
        }
      />
      <div>
        {file.name} <Caption>{file.size} bytes</Caption>
      </div>
    </Stack>
  );

  return (
    
      <DropZone allowMultiple={false} onDrop={handleDropZoneDrop} csv_handler={props.csv_handler}>
        {uploadedFile}
        {fileUpload}
      </DropZone>
    
    
  );
}

function ModalExample(props) {
  const [active, setActive] = useState(false);

  const handleChange = useCallback(() => {
    setActive(!active), [active];
    props.compare();
  });

  const downloadCSV = () => {
    // setActive(!active), [active];
    props.download();
  }

  // let shop_origin_products = 'https://' + props.shop_origin + '/admin/products';
  // let shop_origin_products = '';

  return (
      <Layout.AnnotatedSection
          title="Build new CSV"
          description="Add product photos to CSV."
        >
          <Card sectioned>
            <Button primary onClick={handleChange} disabled={props.is_disabled}>Finish</Button>
          </Card>

          
            <Modal
              open={active}
              onClose={handleChange}
              title="Success! Your new CSV is ready."
              primaryAction={{
                content: 'Download new CSV',
                onAction: downloadCSV,
              }}
            >
              <Modal.Section>
                <TextContainer>
                  <p>
                    To add the photos to your store, head to the <ProductsLink>Products page</ProductsLink> and import the new CSV.
                  </p>
                </TextContainer>
              </Modal.Section>
            </Modal>
          
      </Layout.AnnotatedSection>
  );
}


function SelectExample(props) {
  const [selected, setSelected] = useState('sku');

  const handleSelectChange = useCallback((value) => {
   setSelected(value);
   props.image_convention(value);
  }, []);

  const options = [
    {label: 'SKU', value: 'sku'},
    {label: 'Barcode', value: 'barcode'},
  ];

  if (props.has_db_data) {
    return (
      <Card.Section>
      <Select
        label="Are your photos named using the variant SKU or barcode?"
        options={options}
        onChange={handleSelectChange}
        value={selected}
      />
    </Card.Section>
    );
  } else {
    return null;
  }

}

function FrameOuter(props) {
  const [isLoading, setIsLoading] = useState(false);
  
  const topBarMarkup = (
    <TopBar
    />
  );


  // const loadingMarkup = props.has_trial_response ? <Loading /> : null;
  
  const actualPageMarkup = (
    <Page>
      <Layout>
      

        <UpgradeCard is_trial={props.is_trial}>
        </UpgradeCard>

        <Layout.AnnotatedSection
            title="Get photos"
            description="Connect to your Dropbox account to retrieve photos."
          >
            <ButtonConnect db_data_count={props.db_data_count} image_convention={props.image_convention} has_db_data={props.has_db_data} auth={props.auth} db_handler={props.db_handler} is_trial={props.is_trial}></ButtonConnect>

        </Layout.AnnotatedSection>
        <Layout.AnnotatedSection
          title="Add CSV"
          description="Upload product CSV."
        >

          <Layout>
            <Layout.Section>
              {/* <CSVBanner></CSVBanner> */}
              <NewBanner></NewBanner>
            </Layout.Section>
            <Layout.Section>
              <Card sectioned>
                <Form>
                  <FormLayout>
                    <DropZoneCSV csv_handler={props.csv_handler}>
                    </DropZoneCSV>
                  </FormLayout>
                </Form>
              </Card>
            </Layout.Section>
          </Layout>
        </Layout.AnnotatedSection>
        <ModalExample shop_origin={props.shop_origin} download={props.download} compare={props.compare} is_trial={props.is_trial} is_disabled={props.is_disabled}></ModalExample>
      </Layout>

      <FooterHelp>
        Learn more about{' '}
        <Link url="https://helpcenter.photofinish.io/">
          importing and exporting
        </Link>
      </FooterHelp>
      
    </Page>
  );
  const loadingPageMarkup = (
    <div>
    <Loading />
    <SkeletonPage primaryAction secondaryActions={2}>
      <div></div>
      <Layout>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={8} />
            </TextContainer>
          </Card>
        </Layout.AnnotatedSection>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={8} />
            </TextContainer>
          </Card>
        </Layout.AnnotatedSection>
        <Layout.AnnotatedSection>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={4} />
            </TextContainer>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </SkeletonPage>
    </div>
  );
  const pageMarkup = props.has_trial_response ? actualPageMarkup : loadingPageMarkup;
  const theme = {
    colors: {
      topBar: {
        background: '#FFEA8A',
      },
    },
    logo: {
      width: 124,
      topBarSource:
        'https://cdn.shopify.com/s/files/1/0269/9850/5561/files/Photo-Finish_Final-Logo_Stacked.png?v=1588543926',
      url: 'https://photofinish.io',
      accessibilityLabel: 'Photo Finish',
    },
  };
  return (
    <div>
      <AppProvider
        theme={theme}
      >
        <Frame
          topBar={topBarMarkup}
        >
          {pageMarkup}
        </Frame>
      </AppProvider>
    </div>
  );
}

export default Index;